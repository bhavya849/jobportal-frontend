import AuthLogin from "./Container/Auth/Login";
import AuthRegister from "./Container/Auth/Register";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import AuthForgetPassword from "./Container/Auth/ForgetPassword";
import AuthResetPassword from "./Container/Auth/ResetPassword";
import LandingPage from "./Container/Home/index";
import RecruiterDashboard from "./Container/Recruiter/Dashboard";
import AdminDashboard from "./Container/Admin/Dashboard";
import { ToastContainer } from "react-toastify";
import AdminAuthLogin from "./Container/Admin/Login";
import Dashboard from "./Container/Candidate/Dashboard";

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route exact path="/login" component={AuthLogin} />
          <Route exact path="/admin/login" component={AdminAuthLogin} />
          <Route exact path="/register" component={AuthRegister} />
          <Route exact path="/forget-password" component={AuthForgetPassword} />
          <Route exact path="/reset-password" component={AuthResetPassword} />
          <Route exact path="/home" component={LandingPage}></Route>
          <Route
            exact
            path="/recruiter/dashboard"
            component={RecruiterDashboard}
          />
          <Route exact path="/admin/dashboard" component={AdminDashboard} />
          <Route exact path="/candidate/dashboard" component={Dashboard} />
        </Switch>
        <ToastContainer autoClose={5000}></ToastContainer>
      </div>
    </Router>
  );
}

export default App;
