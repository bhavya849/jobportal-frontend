export const errors = (err) => {
  const messages = {};

  Object.keys(err).forEach((key) => {
    const errorMessage = err[key];

    if (!errorMessage) return "";

    messages[key] =
      typeof errorMessage === "string" ? errorMessage : errorMessage.join(", ");
  });

  return messages;
};

export default errors;
