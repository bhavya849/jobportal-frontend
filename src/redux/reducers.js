import { connectRouter } from "connected-react-router";
import { combineReducers } from "redux";
import recruiter from "./recruiter/reducer";
import auth from "./Auth/reducer";
import admin from "./admin/reducer";
import candidate from "./candidate/reducer";

const reducer = (history) =>
  combineReducers({
    router: connectRouter(history),
    recruiter,
    auth,
    admin,
    candidate,
  });

export default reducer;
