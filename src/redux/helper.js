import { call, put } from "redux-saga/effects";
// import ROUTES from ;
//initialize routes constants
import { errors } from "./errors";
import { toast } from "react-toastify";

export function* doApiCall(service, payload, ACTION_SET_STATE) {
  console.log("api", payload);
  yield showLoading(ACTION_SET_STATE, true);
  yield put({ type: ACTION_SET_STATE, payload: { formErrors: {} } });
  try {
    console.log("payload", payload);
    const response = yield call(service, payload);
    console.log("responsess", response.status);
    yield showLoading(ACTION_SET_STATE, false);
    if (response.status === 204) {
      return { success: true };
    }
    return response.data;
  } catch (e) {
    yield showLoading(ACTION_SET_STATE, false);
    const { response } = e;
    if (response) {
      const { status, data } = response;
      if (status === 422) {
        //initialState ++formErrors
        yield put({
          type: ACTION_SET_STATE,
          payload: { formErrors: errors(data.errors) },
        });
      }
      if (status === 403) {
        //redirect to 403 page
        // yield history.push(ROUTES.NO_ACCESS.path);
      }
    } else {
      //react-toastify
      toast.error(
        "Network Error Detected. Please check your internet connection." +
          e.message,
        { autoClose: false }
      );
    }
  }
  return { success: false };
}

//where to create a loading state
export function* showLoading(action, loading) {
  yield put({ type: action, payload: { loading } });
}
