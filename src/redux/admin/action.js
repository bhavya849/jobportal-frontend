const actions = {
  SET_STATE: "admin/auth/setState",
  LOGIN: "admin/auth/login",
  FETCH_CANDIDATES: "admin/fetchCandidates",
  FETCH_RECRUITERS: "admin/fetchRecruiters",
  FETCH_JOBS: "admin/fetchJobs",
  DELETE_JOBS: "admin/deleteJobs",
  DELETE_CANDIDATE: "admin/deleteCandidate",
  DELETE_RECRUITER: "admin/deleteCandidate",
  GET_APPLICATIONS: "admin/getApplications",
};

export default actions;
