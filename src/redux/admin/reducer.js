import actions from "./action";

const initialState = {
  jobs: [],
  applications: [],
  recruiter: [],
  candidate: [],
  loading: false,
  formErrors: {},
};

const adminReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.SET_STATE:
      return {
        ...state,
        ...action.payload,
      };

    default:
      return state;
  }
};

export default adminReducer;
