import { toast } from "react-toastify";
import { put, all, takeEvery } from "redux-saga/effects";
import AuthService from "../../services/Auth";
import AdminService from "../../services/admin";
import { doApiCall } from "../helper";
import actions from "./action";
import { history } from "../../index";
const auth = new AuthService();
const admin = new AdminService();

export function* LOGIN({ payload: { body } }) {
  const response = yield doApiCall(auth.login, { body }, actions.SET_STATE);
  if (response.success) {
    const { token } = response.data;
    localStorage.token = token;
    yield toast.success("You have successfully logged in");
    yield history.push("/admin/dashboard");
  }
}

export function* FETCH_JOBS({ payload: { body } }) {
  //api call
  const response = yield doApiCall(
    admin.fetchJobs,
    { body },
    actions.SET_STATE
  );

  if (response.success) {
    console.log(response);
    const { data, meta } = response;
    yield put({
      type: actions.SET_STATE,
      payload: { jobs: data, pagination: meta },
    });
    yield toast.success("You have successfully fetched jobs");
    // yield history.push(`/${role}/dashboard`);
  }
}

export function* FETCH_RECRUITERS({ payload: { body } }) {
  //api call
  const response = yield doApiCall(
    admin.fetchRecruiters,
    { body },
    actions.SET_STATE
  );

  if (response.success) {
    console.log(response);

    const { data, meta } = response;
    yield put({
      type: actions.SET_STATE,
      payload: { recruiter: data, pagination: meta },
    });
  }
  yield toast.success("You have successfully fetched recruiters");
}

export function* FETCH_CANDIDATES({ payload: { body } }) {
  //api call
  const response = yield doApiCall(
    admin.fetchCandidates,
    { body },
    actions.SET_STATE
  );

  if (response.success) {
    console.log("candidate", response);
    const { data, meta } = response;
    console.log(data);
    yield put({
      type: actions.SET_STATE,
      payload: { candidate: data, pagination: meta },
    });

    yield toast.success("You have successfully fetched applications");
    // yield history.push(`/${role}/dashboard`);
  }
}

export function* DELETE_JOBS({ payload: { path } }) {
  const response = yield doApiCall(
    admin.deleteJob,
    { path },
    actions.SET_STATE
  );
  if (response.success) {
    console.log(response);
    const { data } = response;
    yield put({
      type: actions.SET_STATE,
      payload: { jobs: data },
    });
    yield toast.success("You have successfully deleted the job");
  }
}

export function* DELETE_CANDIDATE({ payload: { path } }) {
  const response = yield doApiCall(
    admin.deleteCandidate,
    { path },
    actions.SET_STATE
  );
  if (response.success) {
    console.log(response);
    const { data, meta } = response;
    yield put({
      type: actions.SET_STATE,
      payload: { candidate: data, pagination: meta },
    });
    yield toast.success("You have successfully deleted the user");
  }
}

export function* DELETE_RECRUITER({ payload: { path } }) {
  const response = yield doApiCall(
    admin.deleteRecruiter,
    { path },
    actions.SET_STATE
  );
  if (response.success) {
    console.log(response);
    const { data, meta } = response;
    yield put({
      type: actions.SET_STATE,
      payload: { recruiter: data, pagination: meta },
    });
    yield toast.success("You have successfully deleted the user");
  }
}

export function* GET_APPLICATIONS({ path, query }) {
  //get application for a particular candidate
  const response = yield doApiCall(
    admin.getApplicants,
    { path, query },
    actions.SET_STATE
  );

  if (response.success) {
    console.log(response);
    const { data, meta } = response;
    yield put({
      type: actions.SET_STATE,
      payload: { applicants: data, pagination: meta },
    });
  }
}

export default function* rootSaga() {
  yield all([
    takeEvery(actions.LOGIN, LOGIN),
    takeEvery(actions.GET_APPLICATIONS, GET_APPLICATIONS),
    takeEvery(actions.FETCH_JOBS, FETCH_JOBS),
    takeEvery(actions.FETCH_CANDIDATES, FETCH_CANDIDATES),
    takeEvery(actions.FETCH_RECRUITERS, FETCH_RECRUITERS),
    takeEvery(actions.DELETE_JOBS, DELETE_JOBS),
    takeEvery(actions.DELETE_CANDIDATE, DELETE_CANDIDATE),
    takeEvery(actions.DELETE_RECRUITER, DELETE_RECRUITER),
  ]);
}
