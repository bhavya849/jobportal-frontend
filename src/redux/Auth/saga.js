import { toast } from "react-toastify";
import { all, takeEvery } from "redux-saga/effects";
import AuthService from "../../services/Auth";
import { doApiCall } from "../helper";
import actions from "./action";
import * as jwt from "jsonwebtoken";
import { history } from "../../index";
const auth = new AuthService();

export function* LOGIN({ payload: { body } }) {
  console.log("in saga", { body });
  const response = yield doApiCall(auth.loginuser, { body }, actions.SET_STATE);
  console.log("response", response);
  if (response.success) {
    const { token } = response.data;
    const { role } = jwt.decode(token);

    localStorage.token = token;
    //call currAcc
    yield toast.success("You have successfully logged in");
    //not routing succesfully
    yield history.push(`/${role}/dashboard`);
  }
}

//get curr acc

export function* REGISTER({ payload: { body } }) {
  const response = yield doApiCall(auth.register, { body }, actions.SET_STATE);
  if (response.success) {
    yield toast.success("You have successfully registered in");
    //not routing succesfully
    yield history.push(`/login`);
  }
}

export function* FORGETPASSWORD({ payload: { body } }) {
  const response = yield doApiCall(
    auth.forgetPassword,
    { body },
    actions.SET_STATE
  );
  if (response.success) {
    yield history.push("/reset-token-sent");
    //create this page
    //create 404 page
  }
}

export default function* rootSaga() {
  yield all([
    takeEvery(actions.LOGIN, LOGIN),
    takeEvery(actions.REGISTER, REGISTER),
    takeEvery(actions.FORGETPASSWORD, FORGETPASSWORD),
  ]);
}
