const actions = {
  SET_STATE: "auth/setState",
  LOGIN: "auth/login",
  REGISTER: "auth/register",
  FORGETPASSWORD: "auth/forget-password",
  RESETPASSWORD: "auth/reset-password",
};

export default actions;
