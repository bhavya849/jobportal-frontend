import actions from "./action";

const initialState = {
  email: "",
  password: "",
  authorized: false,
  loading: false,
  formErrors: {},
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.SET_STATE:
      return {
        ...state,
        ...action.payload,
      };

    default:
      return state;
  }
};

export default authReducer;
