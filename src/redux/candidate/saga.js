import { all, put, takeEvery } from "redux-saga/effects";
import { toast } from "react-toastify";
import { doApiCall } from "../helper";
import actions from "./action";
import CandidateService from "../../services/candidate";
const candidate = new CandidateService();

export function* FETCH_ALL({ payload: { body } }) {
  //api call
  const response = yield doApiCall(
    candidate.fetchJobs,
    { body },
    actions.SET_STATE
  );

  if (response.success) {
    console.log(response);
    const { data, meta } = response;
    yield put({
      type: actions.SET_STATE,
      payload: { jobs: data, pagination: meta },
    });
    yield toast.success("You have successfully fetched applications");
    // yield history.push(`/${role}/dashboard`);
  }
}

export function* APPLY_JOB({ payload: { path } }) {
  const response = yield doApiCall(
    candidate.applyForJob,
    { path },
    actions.SET_STATE
  );

  if (response.success) {
    console.log(response);
    const { data, meta } = response;
    yield put({
      type: actions.SET_STATE,
      payload: { applications: data, pagination: meta },
    });
    yield toast.success("You have successfully posted the job");
  }
}

export function* GET_APPLICATIONS({ payload: { query } }) {
  const response = yield doApiCall(
    candidate.getApplications,
    { query },
    actions.SET_STATE
  );
  if (response.success) {
    const { data, meta } = response;
    yield put({
      type: actions.SET_STATE,
      payload: { applications: data, pagination: meta },
    });
    yield toast.success("You have successfully fetched your applciations");
  }
}

export default function* rootSaga() {
  yield all([
    takeEvery(actions.FETCH_ALL, FETCH_ALL),
    takeEvery(actions.APPLY_JOB, APPLY_JOB),
    takeEvery(actions.GET_APPLICATIONS, GET_APPLICATIONS),
  ]);
}
