import actions from "./action";

const initialState = {
  jobs: [],
  applications: [],
  loading: false,
  formErrors: {},
};

const candidateReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.SET_STATE:
      return {
        ...state,
        ...action.payload,
      };

    default:
      return state;
  }
};

export default candidateReducer;
