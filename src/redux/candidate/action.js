const actions = {
  SET_STATE: "candidate/setState",
  FETCH_ALL: "candidate/fetchAll",
  APPLY_JOB: "candidate/applyJob",
  GET_APPLICATIONS: "candidate/applications",
};

export default actions;
