const actions = {
  SET_STATE: "recruiter/setState",
  FETCH_ALL: "recruiter/fetchAll",
  POST_JOB: "recruiter/postJob",
  GET_APPLICANTS: "recruiter/getApplicants",
  DELETE_JOB: "recruiter/deleteJob",
};

export default actions;
