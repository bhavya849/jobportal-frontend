import actions from "./action";

const initialState = {
  jobsposted: [],
  jobapplicants: [],
  loading: false,
  formErrors: {},
};

const recruiterReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.SET_STATE:
      return {
        ...state,
        ...action.payload,
      };

    default:
      return state;
  }
};

export default recruiterReducer;
