import { all, put, takeEvery } from "redux-saga/effects";
import { toast } from "react-toastify";
import { doApiCall } from "../helper";
import actions from "./action";
import RecruiterService from "../../services/recruiter";
const recruiter = new RecruiterService();

export function* FETCH_ALL({ payload: { body } }) {
  const response = yield doApiCall(
    recruiter.fetchJobs,
    { body },
    actions.SET_STATE
  );

  if (response.success) {
    console.log(response);
    const { data, meta } = response;
    yield put({
      type: actions.SET_STATE,
      payload: { jobsposted: data, pagination: meta },
    });
    yield toast.success("You have successfully fetched jobs posted by you");
  }
}

export function* POST_JOB({ payload: { body } }) {
  console.log({ body });
  const response = yield doApiCall(
    recruiter.postJob,
    { body },
    actions.SET_STATE
  );

  if (response.success) {
    console.log(response);
    const { data, meta } = response;
    yield put({
      type: actions.SET_STATE,
      payload: { jobsposted: data, pagination: meta },
    });
    yield toast.success("You have successfully posted the job");
  }
}

export function* DELETE_JOB({ payload: { path } }) {
  const response = yield doApiCall(
    recruiter.deleteJob,
    { path },
    actions.SET_STATE
  );
  if (response.success) {
    console.log(response);
    const { data } = response;
    yield put({
      type: actions.SET_STATE,
      payload: { jobsposted: data },
    });

    yield toast.success("You have successfully deleted the job");
  }
}
export function* GET_APPLICANTS({ payload: { path } }) {
  const response = yield doApiCall(
    recruiter.getApplicants,
    { path },
    actions.SET_STATE
  );

  if (response.success) {
    console.log(response);
    const { data, meta } = response;
    yield put({
      type: actions.SET_STATE,
      payload: { jobsapplied: data, pagination: meta },
    });
    yield toast.success(
      "You have successfully fetched applicants for job with jobId:",
      path.id
    );
  }
}

export default function* rootSaga() {
  yield all([
    takeEvery(actions.FETCH_ALL, FETCH_ALL),
    takeEvery(actions.POST_JOB, POST_JOB),
    takeEvery(actions.DELETE_JOB, DELETE_JOB),
    takeEvery(actions.GET_APPLICANTS, GET_APPLICANTS),
  ]);
}
