import { all } from "redux-saga/effects";
import recruiter from "./recruiter/saga";
import auth from "./Auth/saga";
import admin from "./admin/saga";
import candidate from "./candidate/saga";

export default function* rootSaga() {
  yield all([candidate(), auth(), admin(), recruiter()]);
}
