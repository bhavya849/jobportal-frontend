import createSagaMiddleware from "@redux-saga/core";
import { routerMiddleware } from "connected-react-router";
import { createBrowserHistory } from "history";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { toast, ToastContainer } from "react-toastify";
import { logger } from "redux-logger";
import { createStore, applyMiddleware, compose } from "redux";

import App from "./App";
import reducers from "./redux/reducers";
import saga from "./redux/sagas";
import "react-toastify/dist/ReactToastify.css";

const history = createBrowserHistory();
const sagaMiddleware = createSagaMiddleware({
  onError: (err) => {
    toast.error("Something went wrong please try again! ", err);
  },
});

const routeMiddleware = routerMiddleware(history);
const middlewares = [routeMiddleware, sagaMiddleware];

if (process.env.NODE_ENV === "development") {
  middlewares.push(logger);
}

const store = createStore(
  reducers(history),
  compose(applyMiddleware(...middlewares))
);
sagaMiddleware.run(saga);

ReactDOM.render(
  <Provider store={store}>
    <ToastContainer />
    <App history={history} />
  </Provider>,
  document.getElementById("root")
);

export { history, store };
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
