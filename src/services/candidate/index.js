import ApiClient from "../Axios";
import ROUTES from "./constant";
import * as qs from "querystring";

export default class RecruiterService {
  constructor() {
    this.client = ApiClient;
  }

  fetchJobs = async () => {
    return this.client.get(ROUTES.FETCHJOBS, {});
  };

  applyForJob = async ({ path }) => {
    return this.client.post(ROUTES.APPLYJOB.replace(":id", path.id), {});
  };

  getApplications = async ({ query }) => {
    return this.client.get(`${ROUTES.GETAPPLICATIONS}?${qs.stringify(query)}`);
  };
}
