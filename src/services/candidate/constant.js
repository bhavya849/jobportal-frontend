//paths
const ROUTES = {
  APPLYJOB: "/jobs/:id/apply",
  FETCHJOBS: "/jobs",
  GETAPPLICATIONS: "/users/applications",
};

export default ROUTES;
