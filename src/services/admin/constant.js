//paths
const ROUTES = {
  FETCHUSERS: "/admin/users",
  FETCHJOBS: "/admin/jobs",
  GETAPPLICATIONS: "admin/applications/:id",
  DELETEUSER: "/admin/users/:id",
  DELETEJOBS: "/admin/jobs/:id",
};

export default ROUTES;
