import ApiClient from "../Axios";
import ROUTES from "./constant";
import * as qs from "querystring";

export default class RecruiterService {
  constructor() {
    this.client = ApiClient;
  }

  fetchJobs = async () => {
    return this.client.get(ROUTES.FETCHJOBS, {});
  };

  //on the basis of role
  fetchCandidates = async ({ body }) => {
    console.log("body", body);

    return this.client.get(`${ROUTES.FETCHUSERS}?role=candidate`, { body });
  };

  fetchRecruiters = async () => {
    return this.client.get(`${ROUTES.FETCHUSERS}?role=recruiter`, {});
  };

  deleteCandidate = async ({ path }) => {
    return this.client.get(ROUTES.DELETEUSER.replace(":id", path.id), {});
  };

  deleteRecruiter = async ({ path }) => {
    return this.client.get(ROUTES.DELETEUSER.replace(":id", path.id), {});
  };

  deleteJob = async ({ path }) => {
    return this.client.get(ROUTES.DELETEJOBS.replace(":id", path.id), {});
  };

  getApplications = async ({ path, query }) => {
    return this.client.get(
      `${ROUTES.GETAPPLICATIONS.replace(":id", path.id)}?${qs.stringify(
        query
      )}`,
      {}
    );
  };
}
