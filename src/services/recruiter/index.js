import ApiClient from "../Axios";
import ROUTES from "./constant";

export default class RecruiterService {
  constructor() {
    this.client = ApiClient;
  }

  fetchJobs = async () => {
    console.log("fetch posted jobs P.S. push the updated code after check ");
    return this.client.get(ROUTES.FETCHJOBS, {});
  };

  deleteJob = async ({ path }) => {
    return this.client.delete(ROUTES.DELETEJOB.replace(":id", path.id), {});
  };

  postJob = async ({ body }) => {
    console.log("post api call", { body });
    return this.client.post(ROUTES.POSTJOB, body);
  };

  getApplicants = async ({ path }) => {
    return this.client.get(ROUTES.GETAPPLICANTS.replace(":id", path.id), {});
  };
}
