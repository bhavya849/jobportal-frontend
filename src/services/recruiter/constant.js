//paths
const ROUTES = {
  POSTJOB: "/jobs",
  GETAPPLICANTS: "/jobs/:id/applicants",
  FETCHJOBS: "/jobs/posted", //jobs posted by recruiter
  DELETEJOB: "/jobs/:id",
};

export default ROUTES;
