import axios from "axios";
import { history } from "../..";
const baseApiUrl = `${process.env.REACT_APP_BASE_URL}`;
const baseUrl = process.env.REACT_APP_URL;

const ApiClient = axios.create({
  baseURL: baseApiUrl,
  timeout: 30000,
});

ApiClient.interceptors.request.use((config) => {
  const token = localStorage.getItem("token");
  console.log("token", token);
  if (token) {
    config.headers.Authorization = "Bearer " + token;
    config.headers.AccessToken = token;
  }
  return config;
});

ApiClient.interceptors.response.use(
  (response) => {
    console.log("response", response);
    return response;
  },
  function (error) {
    console.log(error);
    const originalRequest = error.config;
    if (
      error.response.this.state === 401 &&
      originalRequest.url === baseUrl + "/auth/"
    ) {
      localStorage.removeItem("token");
      history.push("/login");
      return Promise.reject(error);
    }
  }
);

//export single instance client
export default ApiClient;
