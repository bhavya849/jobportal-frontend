import ApiClient from "../Axios";
import ROUTES from "./constant";

export default class AuthService {
  constructor() {
    this.client = ApiClient;
  }

  loginuser = async ({ body }) => {
    console.log("login api call", { body });
    return this.client.post(ROUTES.LOGIN, body);
  };

  login = async ({ body }) => {
    console.log("login api call", { body });
    return this.client.post(ROUTES.ADMINLOGIN, body);
  };

  register = async ({ body }) => {
    return this.client.post(ROUTES.REGISTER, body);
  };

  forgetPassword = async ({ body }) => {
    return this.client.post(ROUTES.FORGETPASSWORD, body);
  };

  //params mein se email aur token
  resetPassword = async ({ body }) => {
    return this.client.post(ROUTES.RESETPASSWORD, body);
  };
}
