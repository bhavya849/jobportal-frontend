//paths
const ROUTES = {
  ADMINLOGIN: "/admin/auth/login",
  LOGIN: "/auth/login",
  REGISTER: "/auth/register",
  FORGETPASSWORD: "/auth/forget-password",
  RESETPASSWORD: "/auth/reset-password",
};

export default ROUTES;
