import * as React from "react";
import {
  BrowserRouter as Router,
  NavLink,
  Route,
  Switch,
} from "react-router-dom";

import DoubleArrowIcon from "@mui/icons-material/DoubleArrow";
import Button from "@mui/material/Button";
import { Paper, Typography } from "@mui/material";
import { connect } from "react-redux";

import CreateJob from "../../Components/CreateJob";
import actions from "../../redux/recruiter/action";
import JobsPosted from "../../Components/jobsposted";
import Applicants from "../../Components/Applicants";
const mapStateToProps = ({ recruiter, dispatch }) => ({
  recruiter,
  dispatch,
});

const Dashboard = ({ recruiter, dispatch }) => {
  //Dashboard
  const paperStyle = {
    padding: 20,
    height: "150px",
    width: "250px",
    margin: "20px",
  };

  const divStyle = {
    margin: "30px",
    display: "flex",
    flexdirection: "column",
  };

  const fetchJobs = () => {
    dispatch({
      type: actions.FETCH_ALL,
      payload: {},
    });
  };

  const postJob = (payload) => {
    console.log(payload);
    const body = {
      description: payload.description,
      title: payload.title,
      // location: payload.location,
    };
    dispatch({
      type: actions.POST_JOB,
      payload: { body },
    });
  };

  const deleteJob = (id) => {
    dispatch({
      type: actions.DELETE_JOB,
      payload: { path: { id: id } },
    });
  };

  //applicants for particular jobId
  const getApplicants = (jobId, page = 1, perPage = 10) => {
    dispatch({
      type: actions.GET_APPLICANTS,
      payload: { path: { id: jobId }, query: { page, perPage } },
    });
  };

  return (
    <Router>
      <div className="page">
        <Typography mt={1} variant="h3" ml={6}>
          Dashboard
        </Typography>
        <div style={divStyle}>
          <Paper style={paperStyle} elevation={10}>
            <Typography ml={1} variant="button">
              Total number of Jobs Posted
            </Typography>
            <Typography ml={1} variant="h3">
              {recruiter.jobs ? recruiter.jobs.length : 0}
            </Typography>
            <Button
              component={NavLink}
              to="/recruiter/jobs-posted"
              endIcon={<DoubleArrowIcon />}
              onClick={fetchJobs}
            >
              Fetch Jobs
            </Button>
            <Button
              component={NavLink}
              to="/recruiter/create-job"
              endIcon={<DoubleArrowIcon />}
            >
              Create New Job
            </Button>
          </Paper>
        </div>

        <Switch>
          <Route exact path="/recruiter/create-job">
            <CreateJob postJob={postJob} />
          </Route>
          <Route exact path="/recruiter/jobs-posted">
            <JobsPosted
              jobs={recruiter.jobsposted}
              deleteJobs={deleteJob}
              getApplicants={getApplicants}
            />
          </Route>
          <Route exact path="/recrtuiter/applicants">
            <Applicants data={recruiter.jobapplicants}></Applicants>
          </Route>
        </Switch>
      </div>
    </Router>
  );
};

export default connect(mapStateToProps)(Dashboard);
