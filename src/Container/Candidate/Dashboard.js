import * as React from "react";
import {
  BrowserRouter as Router,
  NavLink,
  Route,
  Switch,
} from "react-router-dom";

import DoubleArrowIcon from "@mui/icons-material/DoubleArrow";
import Button from "@mui/material/Button";
import { Typography } from "@mui/material";
import Jobs from "../../Components/Jobs";
import Application from "../../Components/Application";
import actions from "../../redux/candidate/action";
import { connect } from "react-redux";

const mapStateToProps = ({ candidate, dispatch }) => ({
  candidate,
  dispatch,
});

const Dashboard = ({ candidate, dispatch }) => {
  const divStyle = {
    margin: "47px",
    display: "flex",
    flexdirection: "column",
  };

  const getApplications = (page = 1, perPage = 10) => {
    dispatch({
      type: actions.GET_APPLICATIONS,
      payload: { query: { page, perPage } },
    });
  };

  const applyForJob = (id) => {
    dispatch({
      type: actions.APPLY_JOB,
      payload: { path: { id: id } },
    });
  };

  const getJobs = () => {
    dispatch({
      type: actions.FETCH_ALL,
      payload: {},
    });
  };

  return (
    <Router>
      <div className="page">
        <Typography mt={2} variant="h3" ml={6}>
          Dashboard
        </Typography>
        <div style={divStyle}>
          <Button
            variant="outlined"
            component={NavLink}
            to="/candidate/get-jobs"
            endIcon={<DoubleArrowIcon />}
            onClick={getJobs}
          >
            Fetch Jobs
          </Button>
          <Button
            ml={2}
            variant="outlined"
            component={NavLink}
            to="/candidate/get-applications"
            endIcon={<DoubleArrowIcon />}
            onClick={getApplications}
          >
            Get Applications
          </Button>
        </div>

        <Switch>
          <Route exact path="/candidate/get-applications">
            <Application applications={candidate.applications} />
          </Route>
          <Route exact path="/candidate/get-jobs">
            <Jobs apply={applyForJob} jobs={candidate.jobs} />
          </Route>
        </Switch>
      </div>
    </Router>
  );
};

export default connect(mapStateToProps)(Dashboard);
