import { Button, Paper, Typography } from "@mui/material";
import DoubleArrowIcon from "@mui/icons-material/DoubleArrow";
import Recruiter from "../../Components/Recruiter";
import Candidates from "../../Components/Cadidates";
import AdminJobs from "../../Components/AdminJobs";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
} from "react-router-dom";
import { connect } from "react-redux";
import actions from "../../redux/admin/action";

const mapStateToProps = ({ admin, dispatch }) => ({ admin, dispatch });

const AdminDashboard = ({ admin, dispatch }) => {
  const paperStyle = {
    padding: 20,
    height: "20vh",
    width: "20vw",
    margin: "20px",
  };

  const divStyle = {
    margin: "30px",
    display: "flex",
    flexdirection: "column",
  };

  const handleJobs = () => {
    dispatch({
      type: actions.FETCH_JOBS,
      payload: {},
    });
  };

  const handleRecruiters = () => {
    dispatch({
      type: actions.FETCH_RECRUITERS,
      payload: {},
    });
  };

  const handleCandidates = (role = "candidate") => {
    dispatch({
      type: actions.FETCH_CANDIDATES,
      payload: {},
    });
  };

  const deleteRecruiter = (recId) => {
    dispatch({
      type: actions.DELETE_RECRUITER,
      payload: { path: { id: recId } },
    });
  };

  const deleteCandidate = (candId) => {
    dispatch({
      type: actions.DELETE_CANDIDATE,
      payload: { path: { id: candId } },
    });
  };

  const deleteJob = (jobId) => {
    dispatch({
      type: actions.DELETE_JOBS,
      payload: { path: { id: jobId } },
    });
  };

  const getApplications = (candId, page = 1, perPage = 10) => {
    dispatch({
      type: actions.GET_APPLICATIONS,
      payload: { path: { id: candId }, query: { page, perPage } },
    });
    //candidate : button to get applications
  };

  //not implemented backend
  // const getJobsPosted = () => {
  //   //recruiter : button to get jobs posted
  // }

  return (
    <Router>
      <div className="page">
        <Typography mt={1} variant="h3" ml={6}>
          Dashboard
        </Typography>
        <div style={divStyle}>
          <Paper style={paperStyle} elevation={10}>
            <Typography ml={1} variant="button">
              Total number of Jobs Posted
            </Typography>
            <Typography ml={1} variant="h3">
              {23}
            </Typography>
            <Button
              onClick={handleJobs}
              component={NavLink}
              to="/admin/jobs"
              endIcon={<DoubleArrowIcon />}
            >
              Fetch Jobs
            </Button>
          </Paper>

          <Paper style={paperStyle} elevation={10}>
            <Typography ml={1} mt={1} variant="button">
              Total number of Recruiters
            </Typography>
            <Typography ml={1} variant="h3">
              {15}
            </Typography>
            <Button
              onClick={handleRecruiters}
              component={NavLink}
              to="/admin/recruiter"
              endIcon={<DoubleArrowIcon />}
            >
              Fetch Recruiters
            </Button>
          </Paper>
          <Paper mt={1} style={paperStyle} elevation={10}>
            <Typography ml={1} variant="button">
              Total number of Candidates
            </Typography>
            <Typography ml={1} variant="h3">
              {10}
            </Typography>

            <Button
              onClick={handleCandidates}
              component={NavLink}
              to="/admin/candidate"
              endIcon={<DoubleArrowIcon />}
            >
              Fetch Candidates
            </Button>
          </Paper>
        </div>

        <Switch>
          <Route exact path="/admin/candidate">
            <Candidates
              getApplications={getApplications}
              candidate={admin.candidate}
              deleteCandidate={deleteCandidate}
            />
          </Route>
          <Route exact path="/admin/recruiter">
            <Recruiter
              recruiter={admin.recruiter}
              deleteRecruiter={deleteRecruiter}
            />
          </Route>
          <Route>
            <AdminJobs jobs={admin.jobs} deleteJob={deleteJob} />
          </Route>
        </Switch>
      </div>
    </Router>
  );
};

export default connect(mapStateToProps)(AdminDashboard);
