import { Paper, Typography } from "@mui/material";

export default function Application({ applications }) {
  const paperStyle = {
    padding: 20,
    height: "",
    width: "40vw",
    margin: "20px",
  };
  return (
    <div className="jobs">
      <Typography>Applications</Typography>
      {applications?.map((application) => (
        <div className="job-preview" key={application?.id}>
          <Paper elevation={10} style={paperStyle}>
            <h2>{application?.title}</h2>
            <p>{application?.description}</p>
          </Paper>
        </div>
      ))}
    </div>
  );
}
