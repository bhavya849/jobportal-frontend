import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";

export default function Navbar() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" style={{ background: "rgb(74, 103, 233)" }}>
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            Job Portal
          </Typography>

          <Button
            mr={1}
            variant="outlined"
            color="inherit"
            href="http://localhost:3001/register"
          >
            Sign Up
          </Button>
          <Button
            ml={1}
            variant="outlined"
            color="inherit"
            href="http://localhost:3001/login"
          >
            Login
          </Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
