import React from "react";
import { connect } from "react-redux";
import { Grid, Paper, Avatar, Box, TextField, Button } from "@mui/material";

import ResetTvOutlinedIcon from "@mui/icons-material/ResetTvOutlined";
import actions from "../redux/Auth/action";
const mapStateToProps = ({ auth, dispatch }) => ({
  auth,
  dispatch,
});

const ForgetPassword = ({ auth, dispatch }) => {
  const paperStyle = {
    padding: 20,
    height: "40vh",
    width: 420,
    margin: "20px auto",
  };

  const [state, setState] = React.useState({
    email: "",
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch({
      type: actions.FORGETPASSWORD,
      payload: state,
    });
  };

  const handleChange = (e) => {
    setState({
      email: e.target.value,
    });
  };

  const avatarStyle = { backgroundColor: "#1bbd7e" };
  const btnstyle = { margin: "8px 0" };
  return (
    <Grid className="forgetPassword">
      <Paper elevation={10} style={paperStyle}>
        <Grid align="center">
          <Avatar style={avatarStyle}>
            <ResetTvOutlinedIcon />
          </Avatar>
          <h2>Forgot Password</h2>
        </Grid>

        <Box
          component="form"
          sx={{
            "& .MuiTextField-root": { m: 1, width: "50ch" },
          }}
          noValidate
          autoComplete="off"
          onSubmit={handleSubmit}
        >
          <TextField
            fullWidth
            required
            id="filled-required"
            label="Email"
            onChange={handleChange}
            variant="filled"
          />

          <Button
            type="submit"
            color="primary"
            variant="contained"
            style={btnstyle}
            fullWidth
          >
            Send Reset Link
          </Button>
        </Box>
      </Paper>
    </Grid>
  );
};

export default connect(mapStateToProps)(ForgetPassword);
