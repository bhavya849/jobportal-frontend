import React from "react";
import FormPropsTextFieldsLogin from "./loginForm";
import "./../style/form.css";
import { connect } from "react-redux";
import actions from "../redux/admin/action";

const mapStateToProps = ({ auth }) => ({
  formErrors: auth.formErrors,
  loading: auth.loading,
});

const AdminLogin = ({ formErrors, loading, dispatch }) => {
  // console.log("in", formErrors);
  const submit = (values) => {
    const body = {
      email: values.email,
      password: values.password,
    };
    //connect to saga
    // console.log(body);

    dispatch({
      type: actions.LOGIN,
      payload: { body: body },
    });
    console.log(formErrors);
  };

  return (
    <div className="Login">
      <FormPropsTextFieldsLogin onclick={submit} />
    </div>
  );
};

export default connect(mapStateToProps)(AdminLogin);
