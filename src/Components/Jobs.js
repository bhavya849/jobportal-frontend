import { Button, Paper } from "@mui/material";
import DoubleArrowIcon from "@mui/icons-material/DoubleArrow";
import { ToastContainer } from "react-toastify";

export default function Jobs({ jobs }) {
  const paperStyle = {
    padding: 10,
    height: "180px",
    width: "300px",
    margin: "20px",
  };
  // const applyforjob = (id) => {
  //   console.log(id);
  // };

  return (
    <div className="jobs">
      <ToastContainer />
      <Button variant="filled" ml={5}>
        Jobs
      </Button>
      {jobs?.map((job) => (
        <div className="job-preview" key={job?.id}>
          <Paper elevation={1} style={paperStyle}>
            <h2>{job?.title}</h2>
            <p>{job?.description}</p>
            <Button
              variant="outlined"
              endIcon={<DoubleArrowIcon />}
              // onClick={applyforjob(job.id)}
            >
              Apply
            </Button>
          </Paper>
        </div>
      ))}
    </div>
  );
}
