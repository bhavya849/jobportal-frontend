import React from "react";
import { connect } from "react-redux";
import FormPropsTextFields from "./form";
import "./../style/form.css";
import actions from "../redux/Auth/action";

const mapStateToProps = ({ auth }) => ({
  formErrors: auth.formErrors,
  loading: auth.loading,
});

const Register = ({ dispatch, formErrors, loading }) => {
  const onSubmit = (data) => {
    //get data and dispatch
    const body = {
      password: data.password,
      email: data.email,
      firstName: data.firstName,
      lastName: data.lastName,
      username: data.userName,
      role: data.role,
    };

    dispatch({
      type: actions.REGISTER,
      payload: { body: body },
    });
  };
  const copyFormError = {
    ...formErrors,
    email: formErrors.email,
  };
  return (
    <div className="Register">
      <FormPropsTextFields onclick={onSubmit} FormErrors={copyFormError} />
    </div>
  );
};

export default connect(mapStateToProps)(Register);
