import { Button, Paper } from "@mui/material";
import { ToastContainer } from "react-toastify";

import DeleteForeverIcon from "@mui/icons-material/DeleteForever";

export default function AdminJobs({ jobs, deleteJob }) {
  const paperStyle = {
    padding: 10,
    height: "180px",
    width: "300px",
    margin: "20px",
  };

  return (
    <div className="jobs">
      <ToastContainer />
      <Button variant="filled" ml={5}>
        Jobs
      </Button>
      {jobs?.map((job) => (
        <div className="job-preview" key={job?.id}>
          <Paper elevation={1} style={paperStyle}>
            <h2>{job?.title}</h2>
            <p>{job?.description}</p>
            <Button
              variant="outlined"
              endIcon={<DeleteForeverIcon />}
              // onClick={deleteJobs(job.id)}
            >
              Delete
            </Button>
          </Paper>
        </div>
      ))}
    </div>
  );
}
