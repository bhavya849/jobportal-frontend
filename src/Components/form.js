import * as React from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import IconButton from "@mui/material/IconButton";
import FilledInput from "@mui/material/FilledInput";
import InputLabel from "@mui/material/InputLabel";
import InputAdornment from "@mui/material/InputAdornment";
import FormControl from "@mui/material/FormControl";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import MenuItem from "@mui/material/MenuItem";
import Button from "@mui/material/Button";
import { Grid, Paper, Link, Avatar, Typography } from "@mui/material";

import FormHelperText from "@mui/material/FormHelperText";

import AddCircleOutlineOutlinedIcon from "@mui/icons-material/AddCircleOutlineOutlined";

export default function FormPropsTextFields({ onclick, FormErrors }) {
  const roles = [
    {
      value: "candidate",
      label: "Candidate",
    },
    {
      value: "recruiter",
      label: "Recruiter",
    },
  ];

  const [errors, setErrors] = React.useState({
    email: "",
    password: "",
    confirmpassword: "",
    userName: "",
    firstName: "",
    lastName: "",
    role: "",
    candidate: "",
  });

  const [values, setValues] = React.useState({
    firstName: "",
    lastName: "",
    userName: "",
    email: "",
    password: "",
    showPassword: false,
    confirmpassword: "",
    showConfirmPassword: false,
    role: "",
  });

  const handleRoleChange = (e) => {
    setValues({
      ...values,
      role: e.target.value,
    });
  };

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };

  const handleClickShowPassword1 = () => {
    setValues({
      ...values,
      showConfirmPassword: !values.showConfirmPassword,
    });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (validate()) {
      onclick(values);
    }
  };

  const paperStyle = {
    padding: 20,
    height: "800px",
    width: 420,
    margin: "20px auto",
  };
  const avatarStyle = { backgroundColor: "#1bbd7e" };

  const btnstyle = { margin: "8px 0" };

  const validate = () => {
    let temp = [];
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (FormErrors.email) {
      temp.email = "Email already exists";
    }
    temp.email = re.test(values.email) ? "" : "Email is invalid";
    temp.password =
      values.password.length >= 8 ? "" : "Password not big enough";
    temp.confirmpassword =
      values.password === values.confirmpassword
        ? ""
        : "Confirm password should be same as Password";
    const str =
      /^([a-zA-Z]{2,}\s[a-zA-Z]{1,}'?-?[a-zA-Z]{2,}\s?([a-zA-Z]{1,})?)/;
    if (values.firstName.length <= 0) {
      temp.firstName = "cannot be empty";
    }
    if (str.test(values.firstName)) {
      temp.firstName = "Invalid First Name";
    }

    if (values.lastName.length <= 0) {
      temp.lastName = "cannot be empty";
    }
    if (str.test(values.lastName)) {
      temp.lastName = "Invalid First Name";
    }

    const username =
      /([a-zA-Z0-9]([._](?![._])|[a-zA-Z0-9]){6,18}[a-zA-Z0-9]$)/;
    temp.userName =
      values.userName.length > 0 && username.test(values.userName)
        ? ""
        : "Invalid Username";
    setErrors({ ...temp });
    return Object.values(temp).every((x) => x === "");
  };

  return (
    <Grid>
      <p>{FormErrors.email}</p>
      <Paper elevation={10} style={paperStyle}>
        <Grid align="center">
          <Avatar style={avatarStyle}>
            <AddCircleOutlineOutlinedIcon />
          </Avatar>
          <h2>Register</h2>
        </Grid>

        <Box
          component="form"
          sx={{
            "& .MuiTextField-root": { m: 1, width: "50ch" },
          }}
          noValidate
          autoComplete="off"
          onSubmit={handleSubmit}
        >
          <div>
            <FormControl sx={{ m: 1, width: "50ch" }} variant="filled">
              <InputLabel htmlFor="filled-adornment-lastname">
                First Name
              </InputLabel>
              <FilledInput
                {...(errors.firstName && { error: true })}
                id="filled-adornment-firstname"
                type="text"
                value={values.firstName}
                onChange={handleChange("firstName")}
              />
              <FormHelperText id="component-helper-text">
                {errors.firstName}
              </FormHelperText>
            </FormControl>
            <FormControl sx={{ m: 1, width: "50ch" }} variant="filled">
              <InputLabel htmlFor="filled-adornment-email">
                Last Name
              </InputLabel>
              <FilledInput
                {...(errors.lastName && { error: true })}
                id="filled-adornment-lastname"
                type="text"
                value={values.lastName}
                onChange={handleChange("lastName")}
              />
              <FormHelperText id="component-helper-text">
                {errors.lastName}
              </FormHelperText>
            </FormControl>
            <FormControl sx={{ m: 1, width: "50ch" }} variant="filled">
              <InputLabel htmlFor="filled-adornment-username">
                Username
              </InputLabel>
              <FilledInput
                {...(errors.userName && { error: true })}
                id="filled-adornment-username"
                type="text"
                value={values.userName}
                onChange={handleChange("userName")}
              />
              <FormHelperText id="component-helper-text">
                {errors.userName}
              </FormHelperText>
            </FormControl>
            <FormControl sx={{ m: 1, width: "50ch" }} variant="filled">
              <InputLabel htmlFor="filled-adornment-email">Email</InputLabel>
              <FilledInput
                {...(errors.email && { error: true })}
                id="filled-adornment-email"
                type="email"
                value={values.email}
                onChange={handleChange("email")}
              />
              <FormHelperText id="component-helper-text">
                {errors.email}
              </FormHelperText>
            </FormControl>

            <FormControl sx={{ m: 1, width: "50ch" }} variant="filled">
              <InputLabel htmlFor="filled-adornment-password">
                Password
              </InputLabel>
              <FilledInput
                id="filled-adornment-password"
                {...(errors.password && { error: true })}
                type={values.showPassword ? "text" : "password"}
                value={values.password}
                onChange={handleChange("password")}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {values.showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
              />
              <FormHelperText id="component-helper-text">
                {errors.password}
              </FormHelperText>
            </FormControl>

            <FormControl sx={{ m: 1, width: "50ch" }} variant="filled">
              <InputLabel htmlFor="filled-adornment-confirm-password">
                Confirm Password
              </InputLabel>
              <FilledInput
                id="filled-adornment-confirm-password"
                {...(errors.confirmpassword && { error: true })}
                type={values.showConfirmPassword ? "text" : "password"}
                value={values.confirmpassword}
                onChange={handleChange("confirmpassword")}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword1}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {values.showConfirmPassword ? (
                        <VisibilityOff />
                      ) : (
                        <Visibility />
                      )}
                    </IconButton>
                  </InputAdornment>
                }
              />
              <FormHelperText id="component-helper-text">
                {errors.confirmpassword}
              </FormHelperText>
            </FormControl>

            <TextField
              id="filled-select-role"
              select
              label="Select"
              value={values.role}
              onChange={handleRoleChange}
              helperText="Please select your role"
              variant="filled"
            >
              {roles.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
            <Button
              type="submit"
              color="primary"
              variant="contained"
              style={btnstyle}
              fullWidth
            >
              Register
            </Button>
            <Typography>
              Already have an account ?
              <Link href="http://localhost:3001/login">Sign In</Link>
            </Typography>
          </div>
        </Box>
      </Paper>
    </Grid>
  );
}
