import * as React from "react";
import Box from "@mui/material/Box";
import { TextField } from "@mui/material";
import Button from "@mui/material/Button";
import { Grid, Paper } from "@mui/material";

const CreateJob = ({ postJob }) => {
  const btnstyle = { margin: "8px 2px" };
  const paperStyle = {
    padding: 20,
    height: "420px",
    width: "350px",
    margin: "20px auto",
  };

  const [state, setState] = React.useState({
    title: "",
    description: "",
    location: "",
  });

  const handleChange = (props) => (e) => {
    setState({
      ...state,
      [props]: e.target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(state);
    postJob(state);
  };

  return (
    <Grid>
      <Paper style={paperStyle}>
        <Grid align="center">
          <h2>Post Jobs</h2>
        </Grid>
        <Box
          component="form"
          sx={{
            "& .MuiTextField-root": { m: 1, width: "40ch" },
          }}
          noValidate
          autoComplete="off"
          onSubmit={handleSubmit}
        >
          <div className="" align="center">
            <TextField
              required
              id="filled-required"
              label="Job Title"
              variant="filled"
              onChange={handleChange("title")}
            />
            <TextField
              required
              id="outlined-multiline-static"
              label="Job Description"
              multiline
              variant="filled"
              rows={4}
              onChange={handleChange("description")}
            />
            <TextField
              required
              id="filled-required"
              label="Location"
              multiline
              variant="filled"
              onChange={handleChange("location")}
            />
            <Button
              type="submit"
              color="primary"
              variant="contained"
              style={btnstyle}
            >
              Post New Job
            </Button>
          </div>
        </Box>
      </Paper>
    </Grid>
  );
};

export default CreateJob;
