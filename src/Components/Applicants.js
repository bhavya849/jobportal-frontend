import React from "react";
import { Button, Paper } from "@mui/material";
import { ToastContainer } from "react-toastify";
const Applicants = ({ data }) => {
  const paperStyle = {
    padding: 10,
    height: "180px",
    width: "300px",
    margin: "20px",
  };

  return (
    <div className="jobs">
      <ToastContainer />
      <Button variant="filled" ml={5}>
        Applicants
      </Button>
      {data?.map((dt) => (
        //is not returing an id for now will change backend
        <div
          className="job-preview"
          //key={dt?.id}
        >
          <Paper elevation={1} style={paperStyle}>
            <h2>
              {dt?.firstName} {dt?.lastName}
            </h2>
            <p>{dt?.email}</p>
          </Paper>
        </div>
      ))}
    </div>
  );
};

export default Applicants;
