import SportsEsportsIcon from "@mui/icons-material/SportsEsports";
import AppsIcon from "@mui/icons-material/Apps";
import { Button } from "@mui/material";

import landing from "../style/landing.css";

export default function Display() {
  const buttonStyle = {
    color: "#ffffff",
    borderColor: "ffffff",
  };
  return (
    <div className="landing">
      <div className="section-one"></div>
      <div className="section-two">
        <Button
          style={buttonStyle}
          variant="outlined"
          color="inherit"
          ml={3}
          startIcon={<SportsEsportsIcon />}
          fullWidth
        >
          Get it from google play
        </Button>
        <Button
          style={buttonStyle}
          variant="outlined"
          color="inherit"
          startIcon={<AppsIcon />}
          fullWidth
        >
          Get it from App Store
        </Button>
      </div>
    </div>
  );
}
