import { Button, Paper, Typography } from "@mui/material";

import DeleteForeverIcon from "@mui/icons-material/DeleteForever";

export default function Recruiter({ recruiter, deleteRecruiter }) {
  const paperStyle = {
    padding: 20,
    height: "170px",
    width: "300px",
    margin: "20px",
  };
  return (
    <div className="recruiter">
      <Typography>Recruiters</Typography>
      {recruiter?.map((r) => (
        <div className="rec-preview" key={r?.id}>
          <Paper elevation={10} style={paperStyle}>
            <h3>Name: {r?.firstName + " " + r?.lastName}</h3>
            <p>Email ID: {r?.email}</p>
            <p>Username: {r?.username}</p>
            <Button
              variant="outlined"
              endIcon={<DeleteForeverIcon />}
              // onClick={deleteRecruiter(r.id)}
            >
              Delete
            </Button>
          </Paper>
        </div>
      ))}
    </div>
  );
}
