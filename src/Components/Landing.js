import React, { Component } from "react";
import Display from "./Display";
// import Page from './Page';
// import Footer from './Footer';
import Navbar from "./Navbar";

class Landing extends Component {
  render() {
    return (
      <div className="landing-page">
        <Navbar />
        <Display />
      </div>
    );
  }
}
export default Landing;
