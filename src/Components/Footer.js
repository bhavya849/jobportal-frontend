import React, { Component } from 'react';

import { Typography } from '@mui/material';

class Footer extends Component {
  render() {
    return (
      <div>
        <Typography variant="title">Footer Text</Typography>
      </div>
    );
  }
}

export default Footer;
