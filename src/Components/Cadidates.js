import { Button, Paper, Typography } from "@mui/material";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";

export default function Candidate({
  candidate,
  deleteCandidate,
  getApplications,
}) {
  const paperStyle = {
    padding: 20,
    height: "200px",
    width: "250px",
    margin: "20px",
  };

  return (
    <div className="candidate">
      <Typography>Candidates</Typography>
      {candidate?.map((c) => (
        <div className="cand-preview" key={c?.id}>
          <Paper elevation={10} style={paperStyle}>
            <h3>Name: {c?.firstName + " " + c?.lastName}</h3>
            <p>Email ID: {c?.email}</p>
            <p>Username: {c?.username}</p>
            <Button
              variant="outlined"
              endIcon={<DeleteForeverIcon />}
              // onClick={deleteJobs(job.id)}
            >
              Delete
            </Button>
            <Button
              variant="outlined"
              // onClick={deleteJobs(job.id)}
            >
              Get Applications
            </Button>
          </Paper>
        </div>
      ))}
    </div>
  );
}
