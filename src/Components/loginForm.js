import * as React from "react";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import FilledInput from "@mui/material/FilledInput";
import InputLabel from "@mui/material/InputLabel";
import InputAdornment from "@mui/material/InputAdornment";
import FormControl from "@mui/material/FormControl";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Button from "@mui/material/Button";
import { Grid, Paper, Typography, Link } from "@mui/material";
import Avatar from "@mui/material/Avatar";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import FormHelperText from "@mui/material/FormHelperText";

export default function FormPropsTextFieldsLogin({ onclick }) {
  const [values, setValues] = React.useState({
    email: "",
    password: "",
    showPassword: false,
  });

  const [errors, setErrors] = React.useState({
    email: "",
    password: "",
  });

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const paperStyle = {
    padding: 20,
    height: "60vh",
    width: 420,
    margin: "20px auto",
  };

  const onSubmitButton = (e) => {
    e.preventDefault();
    if (validate()) {
      console.log(values);
      onclick(values);
    }
  };

  const avatarStyle = { backgroundColor: "#1bbd7e" };
  const btnstyle = { margin: "8px 0" };

  const validate = () => {
    let temp = [];
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    temp.email = re.test(values.email) ? "" : "Email is invalid";
    temp.password =
      values.password.length >= 8 ? "" : "Password not big enough";
    setErrors({ ...temp });
    return Object.values(temp).every((x) => x === "");
  };

  return (
    <Grid>
      <Paper elevation={10} style={paperStyle}>
        <Grid align="center">
          <Avatar style={avatarStyle}>
            <LockOutlinedIcon />
          </Avatar>
          <h2>Sign In</h2>
        </Grid>
        <Box
          component="form"
          sx={{
            "& .MuiTextField-root": { m: 1, width: "50ch" },
          }}
          noValidate
          autoComplete="off"
          onSubmit={onSubmitButton}
        >
          <div>
            <FormControl sx={{ m: 1, width: "50ch" }} variant="filled">
              <InputLabel htmlFor="filled-adornment-email">Email</InputLabel>
              <FilledInput
                {...(errors.email && { error: true })}
                id="filled-adornment-email"
                type={values.showPassword ? "text" : "email"}
                value={values.email}
                onChange={handleChange("email")}
              />
              <FormHelperText id="component-helper-text">
                {errors.email}
              </FormHelperText>
            </FormControl>
            <FormControl sx={{ m: 1, width: "50ch" }} variant="filled">
              <InputLabel htmlFor="filled-adornment-password">
                Password
              </InputLabel>
              <FilledInput
                {...(errors.password && { error: true })}
                id="filled-adornment-password"
                type={values.showPassword ? "text" : "password"}
                value={values.password}
                onChange={handleChange("password")}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {values.showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
              />

              <FormHelperText id="component-helper-text">
                {errors.password}
              </FormHelperText>
            </FormControl>

            <Button
              type="submit"
              color="primary"
              variant="contained"
              style={btnstyle}
              fullWidth
            >
              Sign in
            </Button>
            <Typography>
              <Link href="http://localhost:3001/forget-password">
                Forgot password ?
              </Link>
            </Typography>
            <Typography>
              Do you have an account ?
              <Link href="http://localhost:3001/register">Sign Up</Link>
            </Typography>
          </div>
        </Box>
      </Paper>
    </Grid>
  );
}
