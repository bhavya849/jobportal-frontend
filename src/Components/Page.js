import { Button, Paper, Typography } from "@mui/material";
import { useState } from "react";
import DoubleArrowIcon from "@mui/icons-material/DoubleArrow";

import Candidates from "./Cadidates";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
} from "react-router-dom";
import Jobs from "./Jobs";
export default function Dashboard() {
  const [candidate, setCandidate] = useState([
    {
      id: 1,
      name: "cand1",
      email: "cand1@gmail.com",
    },
    {
      id: 2,
      name: "bhavya",
      email: "bhavya@gmail.com",
    },
    {
      id: 3,
      name: "ujjwal",
      email: "ujjwal@gmail.com",
    },
  ]);
  const [jobs, setJobs] = useState([
    {
      id: 1,
      title: "UI/UX designer",
      userId: 2,
      description: "user interface designer",
    },
    {
      id: 2,
      title: "Frontend",
      userId: 1,
      description: "Frontend",
    },
    {
      id: 3,
      title: "Backend engineer",
      userId: 1,
      description: "DB designer",
    },
  ]);

  const handleDelete = (id) => {
    const filteredJobs = jobs.filter((job) => job.id !== id);
    setJobs(filteredJobs);
  };

  const handleJobFetch = (e) => {
    //get jobs and render the jobs
    console.log("clicked " + e.target);
  };

  const paperStyle = {
    padding: 20,
    height: "20vh",
    width: "20vw",
    margin: "20px",
  };

  const divStyle = {
    margin: "30px",
    display: "flex",
    flexdirection: "column",
  };

  return (
    <Router>
      <div className="page">
        <Typography mt={1} variant="h3" ml={6}>
          Dashboard
        </Typography>
        <div style={divStyle}>
          <Paper style={paperStyle} elevation={10}>
            <Typography ml={1} variant="button">
              Total number of Jobs Posted
            </Typography>
            <Typography ml={1} variant="h3">
              {23}
            </Typography>
            <Button endIcon={<DoubleArrowIcon />} onClick={handleJobFetch}>
              Fetch Jobs
            </Button>
          </Paper>

          <Paper mt={1} style={paperStyle} elevation={10}>
            <Typography ml={1} variant="button">
              Total number of Candidates
            </Typography>
            <Typography ml={1} variant="h3">
              {10}
            </Typography>

            <Button
              component={NavLink}
              to="/home/candidate"
              endIcon={<DoubleArrowIcon />}
            >
              Fetch Candidates
            </Button>
          </Paper>
        </div>

        <Switch>
          <Route exact path="/home/candidate">
            <Candidates candidate={candidate} />
          </Route>

          <Route>
            <Jobs jobs={jobs} filteredJobs={handleDelete} />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
